<?php

function lawyeria_lite_customizer( $wp_customize ) {

	class lawyeria_lite_Theme_Support extends WP_Customize_Control
	{
		public function render_content()
		{

		}

	} 


    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
    $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
	
	
	
    /*
    ** Header Customizer
    */
    $wp_customize->add_section( 'lawyeria_lite_header_section' , array(
    	'title'       => __( 'Header', 'lawyeria-lite' ),
    	'priority'    => 200,
	) );

		/* Header - Logo */
		$wp_customize->add_setting( 'lawyeria_lite_header_logo',
        array('sanitize_callback' => 'esc_url_raw', 'default' => get_template_directory_uri() .'/images/header-logo.png') );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lawyeria_lite_header_logo', array(
		    'label'    => __( 'Logo:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_header_section',
		    'settings' => 'lawyeria_lite_header_logo',
		    'priority' => '1',
		) ) );

		/* Header - Title */
		$wp_customize->add_setting( 'lawyeria_lite_header_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __('Kontakt','lawyeria-lite')));
		$wp_customize->add_control( 'lawyeria_lite_header_title', array(
		    'label'    => __( 'Contact Title:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_header_section',
		    'settings' => 'lawyeria_lite_header_title',
			'priority' => '2',
		) );

		/* Header - Subtitle */
		$wp_customize->add_setting( 'lawyeria_lite_header_subtitle' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => '03322 / 85 292 46'));
		$wp_customize->add_control( 'lawyeria_lite_header_subtitle', array(
		    'label'    => __( 'Contact telephone:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_header_section',
		    'settings' => 'lawyeria_lite_header_subtitle',
			'priority' => '3',
		) );

    /*
    ** Front Page Customizer
    */
    $wp_customize->add_section( 'lawyeria_lite_frontpage_section' , array(
    	'title'       => __( 'Front Page', 'lawyeria-lite' ),
    	'priority'    => 250,
	) );

		/* Front Page - Contact Form 7 - Title */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_contactform7_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text'));
		$wp_customize->add_control( 'lawyeria_lite_frontpage_contactform7_title', array(
		    'label'    => __( 'Contact Form 7 - Title:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_contactform7_title',
			'priority' => '1',
		) );

		/* Front Page - Contact Form 7 - Shortcode */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_contactform7_shortcode', array('sanitize_callback' => 'laweria_lite_sanitize_shortcode') );
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'lawyeria_lite_frontpage_contactform7_shortcode', array(
		            'label' 	=> __( 'Contact Form 7 - Shortcode:', 'lawyeria-lite' ),
		            'section' 	=> 'lawyeria_lite_frontpage_section',
		            'settings' 	=> 'lawyeria_lite_frontpage_contactform7_shortcode',
		            'priority' 	=> '2'
		        )
		    )
		);

		/* Front Page - SubHeader Title */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_header_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text','default' => __( 'RECHTSANWALT Patrick&nbsp;Hoppe', 'lawyeria-lite' )));
		$wp_customize->add_control( 'lawyeria_lite_frontpage_header_title', array(
		    'label'    => __( 'Subheader Title:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_header_title',
			'priority' => '3',
		) );

		/* Front Page - SubHeader Content */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_header_content' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text','default' => __( 'Ut fermentum aliquam neque, sit amet molestie orci porttitor sit amet. Mauris venenatis et tortor ut ultrices. Nam a neque venenatis, tristique lacus id, congue augue. In id tellus lacus. In porttitor sagittis tellus nec iaculis. Nunc sem odio, placerat a diam vel, varius.', 'lawyeria-lite' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'lawyeria_lite_frontpage_header_content', array(
		            'label' 	=> __( 'Subheader Content:', 'lawyeria-lite' ),
		            'section' 	=> 'lawyeria_lite_frontpage_section',
		            'settings' 	=> 'lawyeria_lite_frontpage_header_content',
		            'priority' 	=> '4'
		        )
		    )
		);

		/* Front Page - Quote */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_subheader_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __( 'Lorem Ipsum is simply dummy text of the printing and type setting industry.', 'lawyeria-lite' )));
		$wp_customize->add_control( 'lawyeria_lite_frontpage_subheader_title', array(
		    'label'    => __( 'Quote:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_subheader_title',
			'priority' => '5',
		) );

		/* Front Page - Subheader Background */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_subheader_bg', array('default' => get_template_directory_uri() . "/images/full-header.jpg", 'sanitize_callback' => 'esc_url_raw') );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lawyeria_lite_frontpage_subheader_bg', array(
		    'label'    => __( 'Subheader Background:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_subheader_bg',
		    'priority' => '6',
		) ) );

		/* Front Page - The Content - Image */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_thecontent_image' ,
        array('default' => get_template_directory_uri().'/images/content-article-image.png','sanitize_callback' => 'esc_url_raw'));
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lawyeria_lite_frontpage_thecontent_image', array(
		    'label'    => __( 'The Content - Image:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_thecontent_image',
		    'priority' => '16',
		) ) );

		/* Front Page - The Content - Title */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_thecontent_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __( 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis.', 'lawyeria-lite' )));
		$wp_customize->add_control( 'lawyeria_lite_frontpage_thecontent_title', array(
		    'label'    => __( 'The Content - Title:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_frontpage_section',
		    'settings' => 'lawyeria_lite_frontpage_thecontent_title',
			'priority' => '17',
		) );

		/* Front Page - The Content - Content */
		$wp_customize->add_setting( 'lawyeria_lite_frontpage_thecontent_content' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __( 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.', 'lawyeria-lite' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'lawyeria_lite_frontpage_thecontent_content', array(
		            'label' 	=> __( 'The Content - Content:', 'lawyeria-lite' ),
		            'section' 	=> 'lawyeria_lite_frontpage_section',
		            'settings' 	=> 'lawyeria_lite_frontpage_thecontent_content',
		            'priority' 	=> '18'
		        )
		    )
		);
		

    /*
    ** 404 Customizer
    */
    $wp_customize->add_section( 'lawyeria_lite_404_section' , array(
    	'title'       => __( '404 Page', 'lawyeria-lite' ),
    	'priority'    => 450,
	) );

		/* 404 - Title */
		$wp_customize->add_setting( 'lawyeria_lite_404_title' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __( '404 Error', 'lawyeria-lite' )));
		$wp_customize->add_control( 'lawyeria_lite_404_title', array(
		    'label'    => __( '404 - Title:', 'lawyeria-lite' ),
		    'section'  => 'lawyeria_lite_404_section',
		    'settings' => 'lawyeria_lite_404_title',
			'priority' => '1',
		) );

		/* 404 - Content */
		$wp_customize->add_setting( 'lawyeria_lite_404_content' ,
        array('sanitize_callback' => 'lawyeria_lite_sanitize_text', 'default' => __( '404 Not Found', 'lawyeria-lite' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'lawyeria_lite_404_content', array(
		            'label' 	=> __( '404 - Content', 'lawyeria-lite' ),
		            'section' 	=> 'lawyeria_lite_404_section',
		            'settings' 	=> 'lawyeria_lite_404_content',
		            'priority' 	=> '2'
		        )
		    )
		);
		
		function lawyeria_lite_sanitize_text( $input ) {
			return wp_kses_post( force_balance_tags( $input ) );
		}

		function lawyeria_lite_sanitize_none( $input ) {
			return $input;
		}
		function laweria_lite_sanitize_shortcode() {
			return force_balance_tags( $input );
		}

}
add_action( 'customize_register', 'lawyeria_lite_customizer' );

/**

 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.

 */

function lawyeria_lite_customize_preview_js() {

	wp_enqueue_script( 'lawyeria_lite_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );

}

add_action( 'customize_preview_init', 'lawyeria_lite_customize_preview_js' );

if( class_exists( 'WP_Customize_Control' ) ):
	class Example_Customize_Textarea_Control extends WP_Customize_Control {
	    public $type = 'textarea';

	    public function render_content() { ?>

	        <label>
	        	<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	        	<textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	        </label>

	        <?php
	    }
	}
endif;

?>
