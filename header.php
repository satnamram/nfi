<?php
/**
 *  The template for displaying Header.
 *
 *  @package lawyeria-lite
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta http-equiv="<?php echo get_template_directory_uri();?>/content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta charset="UTF-8">
		<title><?php wp_title('|', true, 'right'); ?></title>
		
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header>
			<div class="wide-header">
				<div class="wrapper cf">
					<div class="header-left cf">
                            <?php
                            echo '<a class="logo" href="https://secure.webakte.de//e.consult.184428/ExternalRequest/Start?FormId=507325" target="haupt">';
								echo '<img src="'.get_template_directory_uri() .'/images/webakte.png'.'" alt="Webakte"/>';
				            echo '</a>';	
                            ?>
                            <?php
                            echo '<a class="logo" href="https://secure.webakte.de/e.consult.184428" target="haupt">';
								echo '<img src="'.get_template_directory_uri() .'/images/mandantenlogin.jpg'.'" alt="Mandantenlogin"/>';
				            echo '</a>';	
                            ?>
					</div><!--/div .header-left .cf-->
					<div class="header-contact">
    					<?php
    						if ( get_theme_mod( 'lawyeria_lite_header_title','Telefon' ) ) {
    							echo get_theme_mod( 'lawyeria_lite_header_title','Telefon' );
    						}
    					?>
    					<br />
    					<span>
    						<?php
    							if ( get_theme_mod( 'lawyeria_lite_header_subtitle','03322 / 85 292 46' )) { ?>
                                    <a href="tel: <?php echo get_theme_mod( 'lawyeria_lite_header_subtitle','03322 / 85 292 46' ); ?>" title="<?php echo get_theme_mod( 'lawyeria_lite_header_subtitle','03322 / 85 292 46' ); ?>"><?php echo get_theme_mod( 'lawyeria_lite_header_subtitle','03322 / 85 292 46' ); ?></a>
    							<?php }
    						?>
    					</span><!--/span-->
					</div><!--/.header-contact-->
				</div><!--/div .wrapper-->
			</div><!--/div .wide-header-->
            <div id="subheader" style="background-image: url('<?php
				if ( get_theme_mod( 'lawyeria_lite_frontpage_subheader_bg', get_template_directory_uri() . "/images/full-header.jpg" ) ) {
				    echo get_theme_mod( 'lawyeria_lite_frontpage_subheader_bg',get_template_directory_uri() . "/images/full-header.jpg" );
			     }
			 ?>');">
				<div class="subheader-color cf" style="padding-bottom:10px;">
					<div class="wrapper cf">
   						<div class="full-header-content full-header-content-no-sidebar" style="margin-top:30px !important;">
	                       <div style="width:100px;float:left;padding-right:96px;padding-bottom:20px;margin-left:2px;">
                                <?php
                            
								
									echo '<img src="'.get_template_directory_uri() .'/images/pic.jpg'.'" / style="box-shadow: 4px 4px 4px #222;-webkit-border-radius: 1px;-moz-border-radius: 1px;border-radius: 1px;">';
						
                            
							
                                ?>
                            </div>
                            <?php
									if ( get_theme_mod( 'lawyeria_lite_frontpage_header_title','RECHTSANWALT Patrick&nbsp;Hoppe' ) ) {
										echo '<h3>';
										echo get_theme_mod( 'lawyeria_lite_frontpage_header_title','RECHTSANWALT Patrick&nbsp;Hoppe' );
										echo '</h3>';
									}
								?>
								<?php
									if ( get_theme_mod( 'lawyeria_lite_frontpage_header_content','Ut fermentum aliquam neque, sit amet molestie orci porttitor sit amet. Mauris venenatis et tortor ut ultrices. Nam a neque venenatis, tristique lacus id, congue augue. In id tellus lacus. In porttitor sagittis tellus nec iaculis. Nunc sem odio, placerat a diam vel, varius.' )) {
										echo '<p>';
											echo get_theme_mod( 'lawyeria_lite_frontpage_header_content','Ut fermentum aliquam neque, sit amet molestie orci porttitor sit amet. Mauris venenatis et tortor ut ultrices. Nam a neque venenatis, tristique lacus id, congue augue. In id tellus lacus. In porttitor sagittis tellus nec iaculis. Nunc sem odio, placerat a diam vel, varius.' );
										echo '</p>';	
									}
								?>
						</div><!--/div .header-content-->
					</div><!--/div .wrapper-->
				</div><!--/div .full-header-color-->
			</div><!--/div #subheader-->
			<div class="wrapper cf">
			    <nav>
    				<div class="openresponsivemenu">
    					Menü anzeigen
    				</div><!--/div .openresponsivemenu-->
    				<div class="container-menu cf">
        				<?php
        					wp_nav_menu(
        					    array(
        						        'theme_location' => 'header-menu',
        							)
        						);
        					?>
    				</div><!--/div .container-menu .cf-->
    			</nav><!--/nav .navigation-->
		    </div>
			<div class="wrapper">
			<?php 
					$has_header = get_header_image(); 
					if( $has_header ) :
					?>
						<img src="<?php header_image(); ?>" alt="" class="lawyeria-lite-header-image" />
				<?php endif; ?>
			</div>	