		<?php
		/**
		 *  The template for displaying Footer.
		 *
		 *  @package lawyeria-lite
		 */
		?>
		<footer id="footer">
			<div class="wrapper cf">
				<div class="footer-margin-left cf">
                    <div style="float:left;margin:8px;">
                        Patrick Hoppe<br>
                        Rudolf-Virchow-Straße 38<br>
                        14624 Dallgow-Döberitz
                    </div>
                    <div style="float:right;margin:8px;">
                        <br>
                        Ruf   (0 33 22)   2730400<br>
                        Fax   (0 33 22)   2730401
                    </div>
				</div><!--/div .footer-margin-left .cf-->
			</div><!--/div .wrapper .cf-->
		</footer><!--/footer #footer-->
		<?php wp_footer(); ?>
	</body>
</html>
