<?php
/**
 *  The template for displaying Sidebar.
 *
 *  @package lawyeria-lite
 */
?>
<style>
    #posts{
        width:66% !important;
    }
</style>
<aside id="sidebar-right">
	<?php dynamic_sidebar( 'right-sidebar' ); ?>
</aside><!--/aside #sidebar-right-->